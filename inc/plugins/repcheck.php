<?php
if(!defined("IN_MYBB"))
	die("Direct initialization of this file is not allowed.<br /><br />Please make sure IN_MYBB is defined.");

function repcheck_info()
{
	return array(
		"name"			=> "Repcheck",
		"description"	=> "Minimalna ilość reputacji do zakładania wątków",
		"website"		=> "https://www.nowitam.pl",
		"author"		=> "Divir",
		"authorsite"	=> "https://www.nowitam.pl",
		"version"		=> "1.0",
		"guid" 			=> "",
		"compatibility" => "*"
	);
}

function repcheck_is_installed()
{
    global $db;
   
    return $db->num_rows($db->simple_select("settinggroups", "*", "name = \"repcheck\""));
}


function repcheck_install()
{
    global $db;
   
    $settinggroup = [
        "name"              => "repcheck",
        "title"             => "Repcheck",
        "description"       => "Minimalna ilość reputacji do zakładania wątków",
        "isdefault"         => "0",
    ];
   
    $gid = $db->insert_query("settinggroups", $settinggroup);

    $repcheckSettings_1 = array(
        "name"			=> "repcheck_requiredrep",
        "title"			=> "Minimum reputacji",
        "description"	=> "Ile użytkownik musi posiadać reputacji? Domyślnie: 0",
        "optionscode"	=> "numeric",
        "value"			=> "0",
        "disporder"		=> "0",
        "gid"			=> $gid,
	);
	$db->insert_query("settings", $repcheckSettings_1);
    $repcheckSettings_2 = array(
        "name"			=> "repcheck_forums",
        "title"			=> "Uprawnienia forów",
        "description"	=> "Jakie fora brać pod uwagę?",
        "optionscode"	=> "forumselect",
        "value"			=> "-1",
        "disporder"		=> "1",
        "gid"			=> $gid,
	);
	$db->insert_query("settings", $repcheckSettings_2);

    rebuild_settings();
}

function repcheck_activate()
{
}
 
function repcheck_deactivate()
{
    global $db;
   
    $db->delete_query("settinggroups", "name = \"repcheck\"");
    $db->delete_query("settings", "name LIKE \"repcheck%\"");
    rebuild_settings();
}

$plugins->add_hook("datahandler_post_validate_thread", "repcheck");

function repcheck($post)
{
    global $mybb, $forum;
    
    $requiredReputation = $mybb->settings['repcheck_requiredrep'];
    $userReputation = $mybb->user['reputation'];

    $forums = explode (",", $mybb->settings['repcheck_forums']);
    if (in_array($forum['fid'], $forums) || $mybb->settings['repcheck_forums'] == "-1" && $userReputation < $requiredReputation) {
        $post->is_validated = false;
        $post->set_error(sprintf("Nie posiadasz wystarczająco reputacji, aby zakładać wątki w tym dziale. Wymagana wartość: $requiredReputation.", "Wymagana reputacja do pisania to: $requiredReputation"));
    }
}
